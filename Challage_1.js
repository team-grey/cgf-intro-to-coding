function processGrade(grade) {
  // Check if the grade is a failing grade
  if (grade < 40) {
    return { roundedGrade: grade, status: "Failed" };
  }

  // Calculate the next multiple of 5
  let nextMultipleOfFive = Math.ceil(grade / 5) * 5;
  let difference = nextMultipleOfFive - grade;

  // Round up if the difference is less than 3
  if (difference < 3) {
    grade = nextMultipleOfFive;
  }

  // Determine the status based on the rounded grade
  let status;
  if (grade >= 80) {
    status = "Distinction";
  } else if (grade >= 40) {
    status = "Passed";
  } else {
    status = "Failed";
  }

  return { roundedGrade: grade, status: status };
}

// Example grades array
let grades = [38, 42, 43, 77, 79, 82, 85, 90];

// Process and print each grade
grades.forEach((grade) => {
  let result = processGrade(grade);
  console.log(
    `Original Grade: ${grade}, Rounded Grade: ${result.roundedGrade}, Status: ${result.status}`
  );
});