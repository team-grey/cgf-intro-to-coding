function factorial(n) {
  // Initialize lookup table if empty (first run)
  if (typeof lookupTable === 'undefined') {
    lookupTable = {};
    lookupTable[0] = 1; // Factorial of 0 is 1
  }

  // Check if factorial is already in the table
  if (lookupTable[n] !== undefined) {
    return lookupTable[n];
  }

  // Calculate factorial iteratively if not found
  let result = 1;
  for (let i = 1; i <= n; i++) {
    result *= i;
    lookupTable[i] = result; // Store result in table for future lookups
  }

  return result;
}

// Example usage
let number = 5;
let fact = factorial(number);
console.log(`The factorial of ${number} is: ${fact}`);