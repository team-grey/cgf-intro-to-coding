function sortArray(arr) {
    // Sort the array in ascending order
    return arr.sort((a, b) => a - b);
}

// Example usage
let array = [1, 4, 5, 66, 3, 84, 11, 198];
let sortedArray = sortArray(array);
console.log("SORTED:", sortedArray);
