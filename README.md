# The Intro to Coding challenge with Code First Girls
This consisted of 5 challenge:
<br />
* Should we round up the grade
* Sorting an array
* Search a number in a sorted Matrix
* Find factorial of n
* Hiding credit card details

## The Team-Grey

Charlene Grady, Tsion Gebru, Wendy Hallett, Maria Garate

## Dates

14th May 2024 - Challenge Kick-off

16th May 2024 - Teams formed and all participants added to Slack

16th May 2024 - Team and individuals begin to work on challenges

11th June - Team and I submit their challenges responses

14th June - CGF to confirm score

18th June - Finalist presentaions and winner announced

## Challenges

### Challenge 1
Every subject is graded from 0 to 100%. Less than 
40% is failing grade and more than 80% is a 
distinction. 

We can round up a grade:
* If the difference between the  grade and the next multiple of 5  is less than 3, round  up to the next multiple of 5.
* If the value of  is less than 40, no rounding occurs as the result will still be a failing  grade.

Given a input grade, round it up if appropriate and tell us if the student passed, failed or received a distinction.  Write a algorithm and produce a flow chart.

### Challenge 2
You have an array of maximum size of 100 with DISTINCT integers. Write a algorithm and produce a flow chart that sorts this array from smallest to largest. 

EXAMPLE:
[1, 4, 5, 66, 3, 84, 11, 198] 

SORTED:
[1, 3, 4, 5, 11, 66, 84, 198]

### Challenge 3
You are given a matrix (a list of lists) of DISTINCT integers and a target number. Each row in the matrix is SORTED and each column in the matrix is SORTED. Our matrix does not necessarily have the same height and width.

Write a pseudocode and produce a flowchart that:
Finds the number and report back its location (row and column indices of the target integer), if it is contained in the matrix

Otherwise report back that the integer is not in the matrix.

### Challenge 4
What are factorials: https://en.wikipedia.org/wiki/Factorial

The value of n will be small, less than 100. How could we use a lookup table to find the factorial not in the table already. If you would run the program, the first time the look up table would be empty. 


Produce a pseudo code and a flowchart that allows us to find a factorial of a integer n.

### Challenge 5

There are 16 digits on a credit card. Every 4 digits are separated by a space. Start by generating a random credit card number. 
For security reasons, you are going to hide the first 12 digits on the credit card.
Produce a pseudo code and a flowchart.