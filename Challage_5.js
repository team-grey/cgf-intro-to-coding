function generateRandomCreditCardNumber() {
    let cardNumber = '';
    for (let i = 0; i < 16; i++) {
        cardNumber += Math.floor(Math.random() * 10);
        if ((i + 1) % 4 === 0 && i !== 15) {
            cardNumber += ' ';
        }
    }
    return cardNumber;
}

function hideCreditCardNumber(cardNumber) {
    return cardNumber.replace(/\d{4} \d{4} \d{4}/, 'XXXX XXXX XXXX');
}

let randomCardNumber = generateRandomCreditCardNumber();
let hiddenCardNumber = hideCreditCardNumber(randomCardNumber);

console.log("Randomly generated credit card number: " + randomCardNumber);
console.log("After hiding the first 12 digits: " + hiddenCardNumber);