function findTargetInMatrix(matrix, target) {
    // Check if the matrix is empty
    if (matrix.length === 0 || matrix[0].length === 0) {
        console.log("Target not in matrix");
        return;
    }

    let rows = matrix.length;
    let columns = matrix[0].length;

    // Start from the top-right corner of the matrix
    let row = 0;
    let col = columns - 1;

    // Search for the target in the matrix
    while (row < rows && col >= 0) {
        if (matrix[row][col] === target) {
            console.log(`Target `  + target + ` found at row ${row}, column ${col}`);
            return;
        } else if (matrix[row][col] > target) {
            col -= 1; // Move left
        } else {
            row += 1; // Move down
        }
    }

    console.log (target  + " Target not in matrix");
}

// Example matrix and target
let matrix = [
[1,4,7,12,15,1000],
[2,5,19,31,32,1001],
[3,8,24,33,35,1002],
[40,41,42,44,45,1003],
[99,100,103,106,128,1004]
];
let target = 44;

// Call the function
findTargetInMatrix(matrix, target); // Output: Target found at row 1, column 1